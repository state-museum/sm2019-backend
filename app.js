const path = require('path')
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose')
const booksRoutes = require('./routes/books')
const userRoutes = require('./routes/user');
const staffRoutes = require('./routes/staff')


const app = express();

mongoose.connect('mongodb+srv://shadman:handsome@cluster0-n8joz.mongodb.net/mean?retryWrites=true&w=majority')
  .then(() => {
    console.log('Connected to database!')
  })
  .catch(() => {
    console.log('Connection failed!')
  })

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use("/images", express.static(path.join('images')))

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers',
    'Origin X-Requested-With, Content-Type, Accept, Authorization');
  res.setHeader('Access-Control-Allow-Methods',
    'GET, POST, PATCH, PUT, DELETE, OPTIONS');
  next();
})

app.use('/api/books', booksRoutes);
app.use('/api/user', userRoutes);
app.use('/api/staff', staffRoutes);

module.exports = app;