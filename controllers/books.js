const Book = require('../models/book')

exports.createBook = (req, res, next) => {
  const url = req.protocol + '://' + req.get("host")
  const book = new Book({
    isbn: req.body.isbn,
    title: req.body.title,
    author: req.body.author,
    publication: req.body.publication,
    publicationYear: req.body.publicationYear,
    edition: req.body.edition,
    volume: req.body.volume,
    totalPages: req.body.totalPages,
    content: req.body.content,
    media: req.body.media,
    subject: req.body.subject,
    branch: req.body.branch,
    language: req.body.language,
    imagePath: url + "/images/staff/" + req.file.filename
  });

  book.save().then(createdBook => {
    res.status(201).json({
      message: 'Book added successfully',
      book: {
        ...createdBook,
        id: createdBook._id
      }
    });
  });

}

exports.updateBook = (req, res, next) => {
  let imagePath = req.body.imagePath;
  if (req.file) {
    const url = req.protocol + "://" + req.get("host");
    imagePath = url + "/images/" + req.file.filename
  }
  const book = new Book({
    _id: req.body.id,
    isbn: req.body.isbn,
    title: req.body.title,
    author: req.body.author,
    publication: req.body.publication,
    publicationYear: req.body.publicationYear,
    edition: req.body.edition,
    volume: req.body.volume,
    totalPages: req.body.totalPages,
    content: req.body.content,
    media: req.body.media,
    subject: req.body.subject,
    branch: req.body.branch,
    language: req.body.language,
    imagePath: imagePath
  })
  console.log(book)
  Book.updateOne({ _id: req.params.id }, book).then(result => {
    console.log(result);
    res.status(200).json({ message: 'Update successful!' })
  })
}

exports.getBooks = (req, res, next) => {
  const pageSize = +req.query.pageSize;
  const currentPage = +req.query.page;
  const bookQuery = Book.find();
  let fetchedBooks;
  if (pageSize && currentPage) {
    bookQuery
      .skip(pageSize * (currentPage - 1))
      .limit(pageSize);
  }
  bookQuery
    .then(documents => {
      fetchedBooks = documents;
      return Book.count();
    })
    .then(count => {
      res.status(200).json({
        message: 'Books fetched successfully',
        books: fetchedBooks,
        maxBooks: count
      });
    });

}

exports.getBook = (req, res, next) => {
  Book.findById(req.params.id).then(book => {
    if (book) {
      res.status(200).json(book);
    } else {
      res.status(404).json({ message: 'Book not found!' })
    }
  })
}

exports.deleteBook = (req, res, next) => {
  Book.deleteOne({ _id: req.params.id }).then(result => {
    console.log(result);
    res.status(200).json({
      message: 'Post Deleted !'
    });
  });

}