const Staff = require('../models/staff')

exports.createStaff = (req, res, next) => {
  const url = req.protocol + '://' + req.get("host")
  const staff = new Staff({
    adhaarno: req.body.adhaarno,
    empCode: req.body.empCode,
    firstName: req.body.firstName,
    middleName: req.body.middleName,
    lastName: req.body.lastName,
    dob: req.body.dob,
    guardianName: req.body.guardianName,
    identityMark: req.body.identityMark,
    martialStatus: req.body.martialStatus,
    caste: req.body.caste,
    category: req.body.category,
    homeState: req.body.homeState,
    homeDistrict: req.body.homeDistrict,
    homeAddress: req.body.homeAddress,
    homePincode: req.body.homePincode,
    employementType: req.body.employementType,
    phoneNo: req.body.phoneNo,
    alternatePhoneNo: req.body.alternatePhoneNo,
    bloodGroup: req.body.bloodGroup,
    currentDesignation: req.body.currentDesignation,
    staffImagePath: url + "/images/" + req.file.filename,
    employementStatus: req.body.employementStatus
  });
  console.log(staff)
  staff.save().then(createdStaff => {
    res.status(201).json({
      message: 'Staff added successfully',
      staff: {
        ...createdStaff,
        id: createdStaff._id
      }
    })
  })
}

exports.updateStaff = (req, res, next) => {
  let staffImagePath = req.body.staffImagePath;
  if (req.file) {
    const url = req.protocol + "://" + req.file.filename
  }
  const staff = new Staff({
    _id: req.body.id,
    adhaarno: req.body.adhaarno,
    empCode: req.body.empCode,
    firstName: req.body.firstName,
    middleName: req.body.middleName,
    lastName: req.body.lastName,
    dob: req.body.dob,
    guardianName: req.body.guardianName,
    identityMark: req.body.identityMark,
    martialStatus: req.body.martialStatus,
    caste: req.body.caste,
    category: req.body.category,
    homeState: req.body.homeState,
    homeDistrict: req.body.homeDistrict,
    homeAddress: req.body.homeAddress,
    homePincode: req.body.homePincode,
    employementType: req.body.employementType,
    phoneNo: req.body.phoneNo,
    alternatePhoneNo: req.body.alternatePhoneNo,
    bloodGroup: req.body.bloodGroup,
    currentDesignation: req.body.currentDesignation,
    staffImagePath: staffImagePath,
    employementStatus: req.body.employementStatus
  })
  console.log(staff)
  Staff.updateOne({ _id: req.params.id }, staff).then(result => {
    console.log(result);
    res.status(200).json({ message: 'Update successfull' })
  })
}


exports.getStaffs = (req, res, next) => {
  const pageSize = +req.query.pageSize;
  const currentPage = +req.query.page;
  const staffQuery = Staff.find();
  let fetchedStaffs;
  if (pageSize && currentPage) {
    staffQuery
      .skip(pageSize * (currentPage - 1))
      .limit(pageSize)
  }
  staffQuery
    .then(documents => {
      fetchedStaffs = documents;
      return Staff.count();
    })
    .then(count => {
      res.status(200).json({
        message: 'Staffs fetched successfully',
        staffs: fetchedStaffs,
        maxStaffs: count
      })
    })
}

exports.getStaff = (req, res, next) => {
  Staff.findById(req.params.id).then(staff => {
    if (staff) {
      res.status(200).json(staff)
    } else {
      res.status(404).json({
        message: 'Staff not Found!'
      })
    }
  })
}

exports.deleteStaff = (req, res, next) => {
  Staff.deleteOne({ _id: req.params.id }).then(result => {
    console.log(result);
    res.status(200).json({
      message: 'Staff Removed !'
    })
  })
}
