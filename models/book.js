const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);

const bookSchema = mongoose.Schema({
  isbn: {
    type: Number,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  author: {
    type: String,
    required: true
  },
  publication: {
    type: String,
    required: true
  },
  publicationYear: {
    type: Number,
    required: true
  },
  edition: {
    type: Number,
    required: true
  },
  volume: {
    type: Number,
    required: true
  },
  totalPages: {
    type: Number,
    required: true
  },
  media: {
    type: String,
    required: true
  },
  subject: {
    type: String,
    required: true
  },
  branch: {
    type: String,
    required: true
  },
  language: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  imagePath: {
    type: String,
    required: true
  }
});

bookSchema.plugin(AutoIncrement, { inc_field: 'accessionNo' });


module.exports = mongoose.model('Book', bookSchema);