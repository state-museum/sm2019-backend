const mongoose = require('mongoose')

const staffSchema = mongoose.Schema({
  adhaarno: {
    type: Number,
    required: true,
  },
  empCode: {
    type: String,
    required: true
  },
  firstName: {
    type: String,
    required: true,
  },
  middleName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true,
  },
  dob: {
    type: String,
    required: true,
  },
  guardianName: {
    type: String,
    required: true,
  },
  identityMark: {
    type: String,
    required: true,
  },
  martialStatus: {
    type: String,
    required: true,
  },
  caste: {
    type: String,
    required: true,
  },
  category: {
    type: String,
    required: true
  },
  homeState: {
    type: String,
    required: true,
  },
  homeDistrict: {
    type: String,
    required: true,
  },
  homeAddress: {
    type: String,
    required: true
  },
  homePincode: {
    type: Number,
    required: true
  },
  employementType: {
    type: String,
    required: true
  },
  phoneNo: {
    type: Number,
    required: true
  },
  alternatePhoneNo: {
    type: Number,
    required: true
  },
  bloodGroup: {
    type: String,
    required: true,
  },
  currentDesignation: {
    type: String,
    required: true
  },
  staffImagePath: {
    type: String,
    required: true
  },
  employementStatus: {
    type: String,
    required: true
  }
})

module.exports = mongoose.model('Staff', staffSchema)