const express = require('express')
const bookController = require('../controllers/books')
const router = express.Router();


const checkAuth = require('../middleware/check-auth')

const extractFile = require('../middleware/file')

router.put('/:id', checkAuth, extractFile, bookController.updateBook)

router.get('/:id', bookController.getBook)

router.post("", checkAuth, extractFile, bookController.createBook);

router.get('', bookController.getBooks);

router.delete('/:id', checkAuth, bookController.deleteBook)

module.exports = router;