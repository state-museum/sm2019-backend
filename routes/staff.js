const express = require('express')
const staffController = require('../controllers/staff')
const router = express.Router();

const checkAuth = require('../middleware/check-auth')
const extractFile = require('../middleware/file')

router.post('/add-staff', checkAuth, extractFile, staffController.createStaff)

router.put('/add-staff/:id', checkAuth, extractFile, staffController.updateStaff)

router.get('/get-staff/:id', staffController.getStaff)

router.get('/get-staffs', staffController.getStaffs)

router.delete('/get-staff/:id', checkAuth, staffController.deleteStaff)

module.exports = router;